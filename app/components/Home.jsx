var React = require("react");
var Link = require("react-router").Link;

var ListItem = React.createClass({

    addToCart: function () {
        this.props.addItem(this.props.book.id);
        this.props.onCartUpdate();
    },
    render: function () {
        return (
            <div className="col-xs-6 col-md-4">
                <h3>{this.props.book.name}</h3>
                <h4>Price:{this.props.book.price}</h4>
                <Link to={"book/" + this.props.book.id}>Details</Link>
                <button onClick={this.addToCart}>Add to Cart</button>
            </div>
        );
    },
});

var cartItems = [];
var Home = React.createClass({

    getInitialState: function () {
        return ({
            cartItems: this.props.route.cartItems,
        });
    },

    handelUpdate: function () {
        var updateState = this.state.cartItems;
        updateState += 1;
        this.setState({
            cartItems: updateState,
        });
    },
    render: function () {
        return (
            <div className="container-fluid">
                < h1 >LOL Book Store</ h1 >
                <div className="row">
                    {
                        this.props.route.data.map(function (item, i) {
                            return <ListItem addItem={this.props.route.addItem} onCartUpdate={this.handelUpdate} key={i}
                                             book={item}></ListItem>
                        }.bind(this))
                    }
                </div>
                <h4>Recent Cart Items: {this.state.cartItems}</h4>
                <Link to={"cart/"}>My Cart</Link>
            </div>
        );
    },
});
module.exports = Home;
