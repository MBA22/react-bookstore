var React = require('react');

var Order = React.createClass({
    render: function () {
        return (
            <div className="container-fluid">
                <h3>Order Component</h3>
                <div className="row">
                    <div className="col-xs-6">Book</div>
                    <div className="col-xs-6">Quantity</div>
                </div>
                {this.props.route.cartItems.map(function (itemId, i) {
                    return (
                        <div className="row" key={i}>
                            <div className="col-xs-6">{this.props.route.data[i].name}</div>
                            <div className="col-xs-6">{this.props.route.cartItems[i]}</div>
                        </div>
                    );
                }.bind(this))}
            </div>
        );
    },
});

module.exports = Order;
