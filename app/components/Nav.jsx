var React = require("react");
var Link = require("react-router").Link;
var IndexLink = require("react-router").IndexLink;


var Nav = React.createClass({

    render: function () {
        return (
            <div className="container-fluid text-center">
            <nav classNameName="navbar navbar-inverse">
                <ul className="nav navbar-nav">
                    <li>
                        <IndexLink to="/">Home</IndexLink>
                    </li>
                    <li>
                        <Link to="/cart">Cart</Link>
                    </li>
                    <li>
                        <Link to="/order">Order</Link>
                    </li>
                </ul>
            </nav>
            </div>
        );
    },
});
module.exports = Nav;