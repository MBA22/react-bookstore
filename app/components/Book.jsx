var React = require("react");

var Book = React.createClass({
    render: function () {
        var currentBook = this.props.route.data[this.props.params.id];
        return (
            <div className="container-fluid">
                <div className="col-xs-12">
                    <h1>Name: {currentBook.name}</h1>
                    <h3>Pages: {currentBook.pages}</h3>
                    <h3>Price: {currentBook.price}</h3>
                </div>
            </div>
        );
    },
});

module.exports = Book;