var React = require("react");

var Cart = React.createClass({
    render: function () {
        return (
            <div className="container-fluid">
                <div className="row">
                    <h1>Cart Component</h1>
                </div>
                <div className="row">
                    <ul>
                        {this.props.route.cartItems.map(function (itemId, i) {
                            return (
                                <ul key={i}>
                                    <h4 >{this.props.route.data[i].name}</h4>
                                    <li> Quantity: {this.props.route.cartItems[i]}</li>
                                </ul>
                            );
                        }.bind(this))}
                    </ul>
                </div>
            </div>
        );
    },
});

module.exports = Cart;