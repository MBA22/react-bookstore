var React = require('react');
var ReactDOM = require('react-dom');
var {Route, Router, IndexRoute, hashHistory}=require("react-router");
var Main = require('Main');
var Home = require('Home');
var Cart = require('Cart');
var Book = require('Book');
var Order = require('Order');

var booksInCart = [];
var addInCart = function (id) {
    if (!booksInCart[id]) {
        booksInCart[id] = 1
    }
    else {
        booksInCart[id] += 1;
    }
};

var Data = [
    {id: 0, name: 'React Up and Running', pages: '200', price: '400'},
    {id: 1, name: 'React Quickly', pages: '800', price: '1000'},
    {id: 2, name: 'HTML easy', pages: '400', price: '800'},
    {id: 3, name: 'Css Easy', pages: '700', price: '900'},];


ReactDOM.render(
    <Router history={hashHistory}>
        <Route path='/' component={Main}>
            <Route path="cart" cartItems={booksInCart} data={Data} component={Cart}></Route>
            <Route path="book/:id" data={Data} component={Book}></Route>
            <Route path="order" cartItems={booksInCart} data={Data} component={Order}></Route>
            <IndexRoute cartItems={booksInCart.length} addItem={addInCart} data={Data} component={Home}></IndexRoute>
        </Route>
    </Router>,
    document.getElementById('app')
);
